Name:		perl-Text-ParseWords
Version:	3.30
Release:        419
Summary:	Parse text into an array of tokens or array of arrays
License:	GPL+ or Artistic
URL:		https://metacpan.org/release/Text-ParseWords
Source0:	https://cpan.metacpan.org/authors/id/C/CH/CHORNY/Text-ParseWords-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:	perl-interpreter perl-generators
BuildRequires:	perl(ExtUtils::MakeMaker) perl(Test::More)
Requires:	perl(Carp)
Requires:	perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version))

%description
The &nested_quotewords() and &quotewords() functions accept a delimiter (which
can be a regular expression) and a list of lines and then breaks those lines up
into a list of words ignoring delimiters that appear inside quotes. &quotewords()
returns all of the tokens in a single long list, while &nested_quotewords()
returns a list of token lists corresponding to the elements of @lines.
&parse_line() does tokenizing on a single string. The &*quotewords() functions
simply call &parse_line(), so if you're only splitting one line you can call
&parse_line() directly and save a function call.

The $keep argument is a boolean flag. If true, then the tokens are split on the
specified delimiter, but all other characters (including quotes and backslashes)
are kept in the tokens. If $keep is false then the &*quotewords() functions
remove all quotes and backslashes that are not themselves backslash-escaped or
inside of single quotes (i.e., &quotewords() tries to interpret these characters
just like the Bourne shell). NB: these semantics are significantly different
from the original version of this module shipped with Perl 5.000 through 5.004.
As an additional feature, $keep may be the keyword "delimiters" which causes the
functions to preserve the delimiters in each string as tokens in the token lists,
in addition to preserving quote and backslash characters.

%package	help
Summary: 	Doc files for %{name}
Buildarch:	noarch
requires:	man

%description 	help
The %{name}-help package contains doc files for %{name}.

%prep
%setup -q -n Text-ParseWords-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=%{buildroot} NO_PACKLIST=1
find %{buildroot} -type f -name .packlist -exec rm -f {} \;
%{_fixperms} %{buildroot}

%check
make test

%files
%{perl_vendorlib}/*

%files help
%doc CHANGES README
%{_mandir}/man3/*

%changelog
* Tue Sep 17 2019 luhuaxin <luhuaxin@huawei.com> - 3.30-419
- Fix bug: Add buildrequire perl-generators

* Wed Sep 11 2019 luhuaxin <luhuaxin@huawei.com> - 3.30-418
- Package init
